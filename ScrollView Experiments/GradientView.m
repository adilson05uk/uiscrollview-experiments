//
//  GradientView.m
//  JustRideSDK
//
//  Created by Radoslav on 18/02/2019.
//  Copyright © 2019 Masabi. All rights reserved.
//

#import "GradientView.h"

@implementation GradientView {
    CAGradientLayer *_gradientLayer;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updatePoints];
    [self updateLocations];
    [self updateColors];
}

+ (Class)layerClass {
    return [CAGradientLayer class];
}

- (CAGradientLayer *)gradientLayer {
    if (!_gradientLayer) {
        _gradientLayer = (CAGradientLayer *) self.layer;
    }
    return _gradientLayer;
}

- (void)updatePoints {
    [self.gradientLayer setStartPoint:CGPointMake(0.5, 0)];
    [self.gradientLayer setEndPoint:CGPointMake(0.5, 1)];
}

- (void)updateLocations {
    [self.gradientLayer setLocations:@[@0.0, @1.0]];
}

- (void)updateColors {
    [self.gradientLayer setColors:@[(id) self.topColor.CGColor, (id) self.bottomColor.CGColor]];
}

@end
