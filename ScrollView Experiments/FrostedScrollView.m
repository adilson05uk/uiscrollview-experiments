//
//  FrostedScrollView.m
//  JustRideSDK
//
//  Created by Radoslav on 18/02/2019.
//  Copyright © 2019 Masabi. All rights reserved.
//

#import "FrostedScrollView.h"
#import "GradientView.h"

@interface FrostedScrollView () <UIScrollViewDelegate>
@property(weak, nonatomic) IBOutlet UIView *contentView;
@property(weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(weak, nonatomic) IBOutlet UIStackView *stackView;
@property(weak, nonatomic) IBOutlet GradientView *frostedCoverTop;
@property(weak, nonatomic) IBOutlet GradientView *frostedCoverBottom;
@end

@implementation FrostedScrollView {
    CGFloat _scrollThreshold;
    CGFloat _scrollThresholdBottom;
}

# pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    [[NSBundle bundleForClass:self.class] loadNibNamed:@"FrostedScrollView"
                                                 owner:self
                                               options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    self.scrollView.delegate = self;
    _scrollThreshold = 30;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self updateScrollView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateScrollView];
}

# pragma mark - Scroll subviews

- (void)addScrollSubview:(UIView *)subview {
    [self.stackView addArrangedSubview:subview];
    [self updateScrollView];
}

- (void)removeScrollSubview:(UIView *)subview {
    [subview removeFromSuperview];
    [self updateScrollView];
}

- (void)removeAllScrollSubviews {
    for (UIView *subview in self.stackView.subviews) {
        [subview removeFromSuperview];
    }
    [self updateScrollView];
}

- (CGSize)scrollContentSize {
    return self.scrollView.contentSize;
}

# pragma mark - ScrollView

- (void)updateScrollView {
    if ([self isScrollable]) {
        _scrollThresholdBottom = self.stackView.bounds.size.height - self.scrollView.bounds.size.height - _scrollThreshold;
        [self onScroll];
    } else {
        _scrollThresholdBottom = 0;
        self.frostedCoverTop.layer.opacity = 0.0;
        self.frostedCoverBottom.layer.opacity = 0.0;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self onScroll];
}

- (void)onScroll {
    if ([self isScrollable]) {
        CGFloat scrolledTo = self.scrollView.contentOffset.y;

        if (scrolledTo < _scrollThreshold) {
            CGFloat newAlpha = scrolledTo / _scrollThreshold;
            [self.frostedCoverTop setAlpha:newAlpha];
        } else {
            [self.frostedCoverTop setAlpha:1];
        }

        if (scrolledTo > _scrollThresholdBottom) {
            CGFloat unitsScrolledInThreshold = scrolledTo - _scrollThresholdBottom;
            CGFloat newAlpha = 1 - unitsScrolledInThreshold / _scrollThreshold;
            [self.frostedCoverBottom setAlpha:newAlpha];
        } else {
            [self.frostedCoverBottom setAlpha:1];
        }

    }
}

- (Boolean)isScrollable {
    CGFloat scrollViewHeight = self.scrollView.bounds.size.height;
    CGFloat stackViewHeight = self.stackView.bounds.size.height;

    return scrollViewHeight < stackViewHeight;
}

@end
