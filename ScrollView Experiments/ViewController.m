//
//  ViewController.m
//  ScrollView Experiments
//
//  Created by Adil Hussain on 26/02/2019.
//  Copyright © 2019 Masabi Ltd. All rights reserved.
//

#import "ViewController.h"
#import "ViewControllerA.h"
#import "ViewControllerB.h"
#import "ViewControllerC.h"

@interface ViewController ()
@end

@implementation ViewController

- (IBAction)showViewControllerAWithEmptyBodyString:(id)sender {
    ViewControllerA *viewController = [[ViewControllerA alloc] initWithBody:@""];

    [viewController presentFromController:self];
}

- (IBAction)showViewControllerAWithShortBodyString:(id)sender {
    ViewControllerA *viewController = [[ViewControllerA alloc] initWithBody:@"This is a short piece of text within a UIScrollView which will probably only span over two or three lines."];

    [viewController presentFromController:self];
}

- (IBAction)showViewControllerAWithLongBodyString:(id)sender {
    ViewControllerA *viewController = [[ViewControllerA alloc] initWithBody:@"This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling."];

    [viewController presentFromController:self];
}

- (IBAction)showViewControllerBWithEmptyBodyString:(id)sender {
    ViewControllerB *viewController = [[ViewControllerB alloc] initWithBody:@""];

    [viewController presentFromController:self];
}

- (IBAction)showViewControllerBWithShortBodyString:(id)sender {
    ViewControllerB *viewController = [[ViewControllerB alloc] initWithBody:@"This is a short piece of text within a UIScrollView which will probably only span over two or three lines."];

    [viewController presentFromController:self];
}

- (IBAction)showViewControllerBWithLongBodyString:(id)sender {
    ViewControllerB *viewController = [[ViewControllerB alloc] initWithBody:@"This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling."];

    [viewController presentFromController:self];
}

- (IBAction)showViewControllerCWithEmptyBodyString:(id)sender {
    ViewControllerC *viewController = [[ViewControllerC alloc] initWithBody:@""];

    [viewController presentFromController:self];
}

- (IBAction)showViewControllerCWithShortBodyString:(id)sender {
    ViewControllerC *viewController = [[ViewControllerC alloc] initWithBody:@"This is a short piece of text within a UIScrollView which will probably only span over two or three lines."];

    [viewController presentFromController:self];
}

- (IBAction)showViewControllerCWithLongBodyString:(id)sender {
    ViewControllerC *viewController = [[ViewControllerC alloc] initWithBody:@"This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling. This is a long piece of text within a UIScrollView which will enable scrolling."];

    [viewController presentFromController:self];
}

@end
