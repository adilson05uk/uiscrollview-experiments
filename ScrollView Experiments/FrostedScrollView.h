//
//  FrostedScrollView.h
//  JustRideSDK
//
//  Created by Radoslav on 18/02/2019.
//  Copyright © 2019 Masabi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FrostedScrollView : UIView

- (void)addScrollSubview:(UIView *)subview;

- (void)removeScrollSubview:(UIView *)subview;

- (void)removeAllScrollSubviews;

- (void)updateScrollView;

- (CGSize)scrollContentSize;

@end

NS_ASSUME_NONNULL_END
