//
//  ViewControllerC.m
//  ScrollView Experiments
//
//  Created by Adil Hussain on 28/02/2019.
//  Copyright © 2019 Masabi Ltd. All rights reserved.
//

#import "ViewControllerC.h"
#import "FrostedScrollView.h"

@interface ViewControllerC ()
@end

@implementation ViewControllerC {
    NSString *_body;

    __weak IBOutlet UIView *_dimmingView;
    __weak IBOutlet UIStackView *_contentView;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet FrostedScrollView *_scrollView;

    __weak IBOutlet NSLayoutConstraint *_scrollViewHeightConstraint;

    UILabel *_bodyLabel;
}

- (nonnull instancetype)initWithBody:(nonnull NSString *)body {
    if (self = [self init]) {
        _body = body;
    }
    return self;
}

- (void)presentFromController:(nonnull UIViewController *)controller {
    self.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [controller presentViewController:self animated:NO completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self logFrameSizesWithTag:@"viewDidLoad"];

    _titleLabel.text = @"This ViewController demonstrates a custom view which contains a UIScrollView being placed within a UIStackView and being given its height dynamically at runtime based on the size of its content and based on the available space";

    _bodyLabel = [[UILabel alloc] init];
    _bodyLabel.font = [UIFont systemFontOfSize:17];
    _bodyLabel.numberOfLines = 0;
    _bodyLabel.text = _body;
    _bodyLabel.textColor = UIColor.darkTextColor;

    [_scrollView addScrollSubview:_bodyLabel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self logFrameSizesWithTag:@"viewWillAppear"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self logFrameSizesWithTag:@"viewDidAppear"];
    [self resizeScrollView];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];

    _scrollViewHeightConstraint.constant = 0;

    [self logFrameSizesWithTag:@"viewWillTransitionToSize1"];

    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> _Nonnull animation) {
                [self logFrameSizesWithTag:@"viewWillTransitionToSize2"];
                [self resizeScrollView];
            }
                                 completion:^(id <UIViewControllerTransitionCoordinatorContext> _Nonnull context) {
                                     // nothing to do
                                 }];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)resizeScrollView {
    CGFloat dimmingViewMinimumHeight = self.view.frame.size.height * 0.1f;
    CGFloat dimmingViewHeight = _dimmingView.frame.size.height;
    CGFloat scrollViewContentHeight = _scrollView.scrollContentSize.height;

    CGFloat availableHeightForScrollView = MAX(0, dimmingViewHeight - dimmingViewMinimumHeight);
    CGFloat scrollViewHeight = MIN(availableHeightForScrollView, scrollViewContentHeight);

    _scrollViewHeightConstraint.constant = scrollViewHeight;
}

- (void)logFrameSizesWithTag:(NSString *)tag {
    NSLog(@"%@: view.frame is %@", tag, NSStringFromCGRect(self.view.frame));
    NSLog(@"%@: _dimmingView.frame is %@", tag, NSStringFromCGRect(_dimmingView.frame));
    NSLog(@"%@: _contentView.frame is %@", tag, NSStringFromCGRect(_contentView.frame));
    NSLog(@"%@: _scrollView.frame is %@", tag, NSStringFromCGRect(_scrollView.frame));
    NSLog(@"%@: _scrollView.scrollContentSize is %@", tag, NSStringFromCGSize(_scrollView.scrollContentSize));
    NSLog(@"\n");
}

@end
