//
//  AppDelegate.h
//  ScrollView Experiments
//
//  Created by Adil Hussain on 26/02/2019.
//  Copyright © 2019 Masabi Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
