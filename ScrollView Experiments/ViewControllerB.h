//
//  ViewControllerB.h
//  ScrollView Experiments
//
//  Created by Adil Hussain on 26/02/2019.
//  Copyright © 2019 Masabi Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerB : UIViewController

- (nonnull instancetype)initWithBody:(nonnull NSString *)body;

- (void)presentFromController:(nonnull UIViewController *)controller;

@end
