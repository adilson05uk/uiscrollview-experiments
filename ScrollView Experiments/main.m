//
//  main.m
//  ScrollView Experiments
//
//  Created by Adil Hussain on 26/02/2019.
//  Copyright © 2019 Masabi Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
