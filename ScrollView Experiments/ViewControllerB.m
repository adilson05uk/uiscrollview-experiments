//
//  ViewControllerB.m
//  ScrollView Experiments
//
//  Created by Adil Hussain on 26/02/2019.
//  Copyright © 2019 Masabi Ltd. All rights reserved.
//

#import "ViewControllerB.h"

@interface ViewControllerB ()
@end

@implementation ViewControllerB {
    NSString *_body;

    __weak IBOutlet UIView *_dimmingView;
    __weak IBOutlet UIStackView *_contentView;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UIScrollView *_scrollView;
    __weak IBOutlet UILabel *_bodyLabel;

    __weak IBOutlet NSLayoutConstraint *_scrollViewHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *_safeAreaBottomToStackViewBottomConstraint;
}

- (nonnull instancetype)initWithBody:(nonnull NSString *)body {
    if (self = [self init]) {
        _body = body;
    }
    return self;
}

- (void)presentFromController:(nonnull UIViewController *)controller {
    self.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [controller presentViewController:self animated:NO completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self logFrameSizesWithTag:@"viewDidLoad"];

    _titleLabel.text = @"This ViewController demonstrates a UIScrollView being placed within a UIStackView and being given its height dynamically at runtime based on the size of its content and based on the available space";

    _bodyLabel.text = _body;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self logFrameSizesWithTag:@"viewDidAppear1"];

    [self resizeScrollView];

    [self.view layoutIfNeeded];

    [self logFrameSizesWithTag:@"viewDidAppear2"];

    _contentView.hidden = NO;

    CGFloat stackViewHeight = self->_contentView.frame.size.height;
    _safeAreaBottomToStackViewBottomConstraint.constant = -stackViewHeight;

    [self.view layoutIfNeeded];

    [self logFrameSizesWithTag:@"viewDidAppear3"];

    [self performSlideInAnimation];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];

    _scrollViewHeightConstraint.constant = 0;

    [self logFrameSizesWithTag:@"viewWillTransitionToSize1"];

    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> _Nonnull animation) {
                [self logFrameSizesWithTag:@"viewWillTransitionToSize2"];
                [self resizeScrollView];
            }
                                 completion:^(id <UIViewControllerTransitionCoordinatorContext> _Nonnull context) {
                                     // nothing to do
                                 }];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)resizeScrollView {
    CGFloat dimmingViewMinimumHeight = self.view.frame.size.height * 0.1f;
    CGFloat dimmingViewHeight = _dimmingView.frame.size.height;
    CGFloat scrollViewContentHeight = _scrollView.contentSize.height;

    CGFloat availableHeightForScrollView = MAX(0, dimmingViewHeight - dimmingViewMinimumHeight);
    CGFloat scrollViewHeight = MIN(availableHeightForScrollView, scrollViewContentHeight);

    _scrollViewHeightConstraint.constant = scrollViewHeight;
}

- (void)performSlideInAnimation {
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self logFrameSizesWithTag:@"animation"];
                         self->_safeAreaBottomToStackViewBottomConstraint.constant = 0;
                         [self.view layoutIfNeeded];
                     }];
}

- (void)logFrameSizesWithTag:(NSString *)tag {
    NSLog(@"%@: view.frame is %@", tag, NSStringFromCGRect(self.view.frame));
    NSLog(@"%@: _dimmingView.frame is %@", tag, NSStringFromCGRect(_dimmingView.frame));
    NSLog(@"%@: _contentView.frame is %@", tag, NSStringFromCGRect(_contentView.frame));
    NSLog(@"%@: _scrollView.frame is %@", tag, NSStringFromCGRect(_scrollView.frame));
    NSLog(@"%@: _scrollView.contentSize is %@", tag, NSStringFromCGSize(_scrollView.contentSize));
    NSLog(@"\n");
}

@end
