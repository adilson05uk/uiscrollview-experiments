//
//  GradientView.h
//  JustRideSDK
//
//  Created by Radoslav on 18/02/2019.
//  Copyright © 2019 Masabi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GradientView : UIView

@property(nonatomic) IBInspectable UIColor *topColor;
@property(nonatomic) IBInspectable UIColor *bottomColor;

@end

NS_ASSUME_NONNULL_END
